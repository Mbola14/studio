<%@page import="publics.models.*, java.util.ArrayList" %>
<%
    ArrayList plateau=(ArrayList) request.getAttribute("plateau");
    ArrayList temps=(ArrayList) request.getAttribute("temps");
    ArrayList etat=(ArrayList) request.getAttribute("etat");
%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Scene</title>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/resources/theme/css/simplebar.css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/resources/theme/css/feather.css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/resources/theme/css/daterangepicker.css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/resources/theme/css/app-light.css" id="lightTheme">
</head>
<body class="light ">
<div class="wrapper vh-100">
    <div class="row align-items-center h-100">
        <form class="col-lg-6 col-md-8 col-10 mx-auto" action="<%=request.getContextPath()%>/scenes" method="post">
            <div class="mx-auto text-center my-4">
                <h2 class="my-3">Insertion Scène</h2>
            </div>
            <div class="form-group">
                <label for="plateau">Plateau</label>
                <select class="custom-select " id="plateau" name="plateau" >
                    <option selected>Choisir plateau</option>
                    <% for(int i=0;i< plateau.size();i++){
                        Plateau pl=(Plateau) plateau.get(i);
                        %>
                    <option value="<%= pl.getIdPlateau() %>"><%= pl.getNom() %></option>
                    <% } %>
                </select>
            </div>
            <div class="form-group">
                <label for="duration">Duration</label>
                <input type="number" name="duration" id="duration" class="form-control" min="0" step="1">
            </div>
            <div class="form-group">
                <label for="temps">Temps</label>
                <select class="custom-select " id="temps" name="temps" >
                    <option selected>Choisir la période</option>
                    <% for(int i=0;i<temps.size();i++){
                        Temps t=(Temps) temps.get(i);
                        %>
                    <option value="<%=t.getIdTemps()%>"><%=t.getPeriode()%></option>
                    <% } %>
                </select>
            </div>
            <div class="form-group">
                <label for="etat">Etat</label>
                <select class="custom-select " id="etat" name="etat" >
                    <option selected>Choisir l'état</option>
                    <% for(int i=0;i<etat.size();i++){
                        Etat e=(Etat) etat.get(i);
                        %>
                    <option value="<%=e.getIdEtat()%>"><%=e.getEtat()%></option>
                    <% } %>
                </select>
            </div>
            <div class="form-group">
                <label for="planification">Planification</label>
                <input type="datetime-local" name="plan" id="planification" class="form-control">
            </div>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Valider</button>
        </form>
    </div>
</div>

    <script src="<%=request.getContextPath()%>/resources/theme/js/jquery.min.js"></script>
    <script src="<%=request.getContextPath()%>/resources/theme/js/popper.min.js"></script>
    <script src="<%=request.getContextPath()%>/resources/theme/js/moment.min.js"></script>
    <script src="<%=request.getContextPath()%>/resources/theme/js/bootstrap.min.js"></script>
    <script src="<%=request.getContextPath()%>/resources/theme/js/simplebar.min.js"></script>
    <script src="<%=request.getContextPath()%>/resources/theme/js/daterangepicker.js"></script>
    <script src="<%=request.getContextPath()%>/resources/theme/js/jquery.stickOnScroll.js"></script>
    <script src="<%=request.getContextPath()%>/resources/theme/js/tinycolor-min.js"></script>
    <script src="<%=request.getContextPath()%>/resources/theme/js/config.js"></script>
    <script src="<%=request.getContextPath()%>/resources/theme/js/apps.js"></script>
</body>
</html>
