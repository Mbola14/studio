<%@page import="publics.models.*, java.util.ArrayList" %>
<%
    ArrayList acteur=(ArrayList) request.getAttribute("acteur");
    int idscene=(int) request.getAttribute("idscene");
%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Action Form</title>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/resources/theme/css/simplebar.css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/resources/theme/css/feather.css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/resources/theme/css/daterangepicker.css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/resources/theme/css/app-light.css" id="lightTheme">
</head>
<body class="light ">
<div class="wrapper vh-100">
    <div class="row align-items-center h-100">
        <form class="col-lg-6 col-md-8 col-10 mx-auto" action="<%=request.getContextPath()%>/actions" method="post">
            <div class="mx-auto text-center my-4">
                <h2 class="my-3">Insertion Action</h2>
            </div>
            <div class="form-group">
                <input type="hidden" name="scene" value="<%=idscene%>">
                <label for="acteur">Acteur</label>

                <select class="custom-select " id="acteur" name="acteur" >
                    <option selected>Choisir l'acteur</option>
                    <% for(int i=0;i<acteur.size();i++){
                        Acteur a=(Acteur) acteur.get(i);
                    %>
                    <option value="<%=a.getIdActeur()%>"><%=a.getNom()%></option>
                    <% } %>
                </select>
            </div>
            <div class="form-group">
                <label for="dialogue">Dialogue</label>
                <textarea class="form-control" id="dialogue" name="dialogue" cols="50" rows="5"></textarea>
            </div>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Valider</button>
        </form>
    </div>
</div>


    <script src="<%=request.getContextPath()%>/resources/theme/js/jquery.min.js"></script>
    <script src="<%=request.getContextPath()%>/resources/theme/js/popper.min.js"></script>
    <script src="<%=request.getContextPath()%>/resources/theme/js/moment.min.js"></script>
    <script src="<%=request.getContextPath()%>/resources/theme/js/bootstrap.min.js"></script>
    <script src="<%=request.getContextPath()%>/resources/theme/js/simplebar.min.js"></script>
    <script src="<%=request.getContextPath()%>/resources/theme/js/daterangepicker.js"></script>
    <script src="<%=request.getContextPath()%>/resources/theme/js/jquery.stickOnScroll.js"></script>
    <script src="<%=request.getContextPath()%>/resources/theme/js/tinycolor-min.js"></script>
    <script src="<%=request.getContextPath()%>/resources/theme/js/config.js"></script>
    <script src="<%=request.getContextPath()%>/resources/theme/js/apps.js"></script>
</body>
</html>
