<%@page import="publics.models.*, java.util.ArrayList" %>
<%
    ArrayList action=(ArrayList) request.getAttribute("action");
%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Action List</title>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/resources/theme/css/simplebar.css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/resources/theme/css/feather.css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/resources/theme/css/daterangepicker.css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/resources/theme/css/app-light.css" id="lightTheme">
</head>
<body class="vertical light">
<main role="main" class="main-content">
    <div class="container-fluid">
        <div class="row justify-content-center mx-auto">
            <div class="col-12">
                <h1 class="page-title">Liste action</h1>
                <div class="row">
                    <% for(int i=0;i< action.size();i++){
                        Action a=(Action) action.get(i);
                    %>
                    <div class="col-md-4 mb-4">
                        <div class="card shadow">
                            <div class="card-header">
                                <strong class="card-title">Acteur : <%=a.getIdActeur().getNom()%></strong>
                            </div>
                            <div class="card-body">
                                <p>Dialogue</p>
                                <textarea class="form-control" cols="20" rows="2" disabled><%=a.getDialogue()%></textarea>
                            </div>
                        </div>
                    </div>
                    <% } %>
                </div>
                <% if(!action.isEmpty()){
                    Action ac=(Action) action.get(0);
                %>
                    <h2 class="h5">Scène détails</h2>
                    <p class="text-muted">Plateau : <%=ac.getIdScene().getIdPlateau().getNom()%></p>
                    <p class="text-muted">Duration : <%=ac.getIdScene().getDuration()%></p>
                    <p class="text-muted">Temps : <%=ac.getIdScene().getIdTemps().getPeriode()%></p>
                    <p class="text-muted">Etat : <%=ac.getIdScene().getIdEtat().getEtat()%></p>
                    <a class="btn mb-2 btn-outline-info" href="<%=request.getContextPath()%>/actions/form/<%=ac.getIdScene().getIdScene()%>">Ajouter Action</a>
                    <% } %>
            </div>
        </div>
    </div>

</main>

    <script src="<%=request.getContextPath()%>/resources/theme/js/jquery.min.js"></script>
    <script src="<%=request.getContextPath()%>/resources/theme/js/popper.min.js"></script>
    <script src="<%=request.getContextPath()%>/resources/theme/js/moment.min.js"></script>
    <script src="<%=request.getContextPath()%>/resources/theme/js/bootstrap.min.js"></script>
    <script src="<%=request.getContextPath()%>/resources/theme/js/simplebar.min.js"></script>
    <script src="<%=request.getContextPath()%>/resources/theme/js/daterangepicker.js"></script>
    <script src="<%=request.getContextPath()%>/resources/theme/js/jquery.stickOnScroll.js"></script>
    <script src="<%=request.getContextPath()%>/resources/theme/js/tinycolor-min.js"></script>
    <script src="<%=request.getContextPath()%>/resources/theme/js/config.js"></script>
    <script src="<%=request.getContextPath()%>/resources/theme/js/apps.js"></script>
</body>
</html>
