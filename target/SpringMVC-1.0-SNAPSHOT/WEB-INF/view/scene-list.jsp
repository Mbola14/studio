<%@page import="publics.models.*, java.util.ArrayList" %>
<%
    ArrayList scene=(ArrayList) request.getAttribute("scene");
    ArrayList etat=(ArrayList) request.getAttribute("etat");
    int idetat=(int) request.getAttribute("idetat");
    int nombrepage=(int) request.getAttribute("nombrepage");
%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Scene List</title>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/resources/theme/css/simplebar.css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/resources/theme/css/feather.css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/resources/theme/css/daterangepicker.css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/resources/theme/css/app-light.css" id="lightTheme">
</head>
<body class="vertical light">
<main role="main" class="main-content">
    <div class="container-fluid">
        <div class="row justify-content-center mx-auto">
            <div class="col-12">
                <div class="row align-items-center my-4">
                    <div class="col">
                        <h2 class="h3 mb-0 page-title">Liste Scène</h2>
                    </div>

                    <div class="col-auto file-action">
                        <button type="button" class="btn btn-link dropdown-toggle more-vertical p-0 text-muted mx-auto" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="fe fe-filter fe-12 mr-2"></span>Filtrer
                        </button>
                        <div class="dropdown-menu m-2">
                            <% for(int i=0;i< etat.size();i++){
                                Etat e=(Etat) etat.get(i);
                            %>
                            <a class="dropdown-item" href="<%=request.getContextPath()%>/scenes/etat/<%=e.getIdEtat()%>/1"><i class="fe fe-circle fe-12 mr-4"></i><%=e.getEtat()%></a>
                            <% } %>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <% for(int i=0;i< scene.size();i++){
                        Scene s=(Scene) scene.get(i);
                    %>
                    <div class="col-md-4 mb-4">
                        <div class="card shadow">

                            <div class="card-body">
                                <div class="tab-content">
                                    <p class="small"><span class="fe fe-10 fe-film"></span> Plateau : <%=s.getIdPlateau().getNom()%></p>
                                    <p class="small"><span class="fe fe-10 fe-clock"></span> Durée : <%=s.getDuration()%></p>
                                    <p class="small"><span class="fe fe-10 fe-cloud"></span> Période : <%=s.getIdTemps().getPeriode()%></p>
                                    <p class="small"><span class="fe fe-10 fe-clock"></span> Planification : <% if(s.getPlanification()!=null){ out.print(s.getPlanification()); }%></p>
                                    <p class="small"><small><span class="dot dot-lg bg-secondary mr-1"></span> <%=s.getIdEtat().getEtat()%> </small></p>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row align-items-center justify-content-between">
                                    <a class="btn mb-2 btn-outline-link" href="<%=request.getContextPath()%>/actions/form/<%=s.getIdScene()%>">Ajout Action</a>
                                    <a class="btn mb-2 btn-outline-link" href="<%=request.getContextPath()%>/actions/<%=s.getIdScene()%>">Liste Action</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <% } %>
                </div>
                <nav aria-label="Table Paging" class="my-3">
                    <ul class="pagination justify-content-end mb-0">
                        <% for(int i=0;i<nombrepage;i++){ %>
                            <% if(idetat==0){ %>
                                <li class="page-item"><a class="page-link" href="<%=request.getContextPath()%>/scenes/<%=i+1%>"><%=i+1%></a></li>
                            <% }else{ %>
                                <li class="page-item"><a class="page-link" href="<%=request.getContextPath()%>/scenes/etat/<%=idetat%>/<%=i+1%>"><%=i+1%></a></li>
                            <% } %>
                        <% } %>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</main>

    <script src="<%=request.getContextPath()%>/resources/theme/js/jquery.min.js"></script>
    <script src="<%=request.getContextPath()%>/resources/theme/js/popper.min.js"></script>
    <script src="<%=request.getContextPath()%>/resources/theme/js/moment.min.js"></script>
    <script src="<%=request.getContextPath()%>/resources/theme/js/bootstrap.min.js"></script>
    <script src="<%=request.getContextPath()%>/resources/theme/js/simplebar.min.js"></script>
    <script src="<%=request.getContextPath()%>/resources/theme/js/daterangepicker.js"></script>
    <script src="<%=request.getContextPath()%>/resources/theme/js/jquery.stickOnScroll.js"></script>
    <script src="<%=request.getContextPath()%>/resources/theme/js/tinycolor-min.js"></script>
    <script src="<%=request.getContextPath()%>/resources/theme/js/config.js"></script>
    <script src="<%=request.getContextPath()%>/resources/theme/js/apps.js"></script>
</body>
</html>
