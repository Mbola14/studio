<%
    String erreur= (String)request.getAttribute("error");
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Rezume Free Template by Colorlib</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/theme/css/animate.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/theme/css/flexslider.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/theme/fonts/icomoon/style.css">

    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/theme/css/bootstrap.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/theme/css/style.css">

    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,700" rel="stylesheet">


</head>
<body data-spy="scroll" data-target="#pb-navbar" data-offset="200">


<section class="site-section" id="section-contact">
    <div class="container">
        <div class="row">
            <div class="col-md-12 mb-5">
                <div class="section-heading text-center">
                    <h2> <strong>Login Admin</strong></h2>
                </div>
            </div>

            <div class="col-md-12 mb-5 mb-md-0">
                <form action="${pageContext.request.contextPath}/admin/login" method="post" class="site-form" >
                    <div class="form-group">
                        <p>Nom: <input name="nom" type="text" class="form-control px-3 py-4" placeholder="Nom"></p>
                    </div>
                    <div class="form-group">
                        <p>Mot de passe <input name="mdp" type="password" class="form-control px-3 py-4" placeholder="Mot de passe"></p>
                    </div>
                    <div class="form-group">
                        <div class="section-heading text-center">
                        <input type="submit" class="btn btn-primary  px-4 py-3" value="Se connecter">
                        </div>
                    </div>
                </form>
                <% if(erreur!=null) { %>
                <h3>Mot de passe ou Nom incorrecte</h3>
                <% } %>
            </div>
        </div>
    </div>
</section>



<script src="${pageContext.request.contextPath}/resources/theme/js/vendor/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/theme/js/vendor/jquery-migrate-3.0.1.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/theme/js/vendor/popper.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/theme/js/vendor/bootstrap.min.js"></script>

<script src="${pageContext.request.contextPath}/resources/theme/js/vendor/jquery.easing.1.3.js"></script>

<script src="${pageContext.request.contextPath}/resources/theme/js/vendor/jquery.stellar.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/theme/js/vendor/jquery.waypoints.min.js"></script>

<script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js"></script>
<script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
<script src="js/custom.js"></script>

</script>


<!-- Google Map -->
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
    <script src="js/google-map.js"></script> -->

</body>
</html>