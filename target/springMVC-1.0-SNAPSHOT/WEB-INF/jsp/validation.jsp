<%@ page import="com.example.models.Contenue" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%
    List<Contenue> info=(List<Contenue>) request.getAttribute("info");
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Rezume Free Template by Colorlib</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/theme/css/animate.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/theme/css/flexslider.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/theme/fonts/icomoon/style.css">

    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/theme/css/bootstrap.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/theme/css/style.css">

    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,700" rel="stylesheet">


</head>
<body data-spy="scroll" data-target="#pb-navbar" data-offset="200">

<div class="section-heading text-center">
    <div class="filters">
        <ul>
            <a href="${pageContext.request.contextPath}/admin"><li class="active" >Modification</li></a>
        </ul>
    </div>
</div>

<section class="site-section" id="section-blog">
    <div class="container">
        <div class="row">
            <div class="col-md-12 mb-5">
                <div class="section-heading text-center">
                    <h2><strong>Validation des actualités</strong></h2>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 mb-5">
                <div class="row">
                    <div class="col-sm-6 col-lg-4 mb-4">
                        <p></p>
                    </div>
                    <div class="col-sm-6 col-lg-4 mb-4">
                        <div class="section-heading text-center">

                            <div class="form-group">
                                <form method="post" action="${pageContext.request.contextPath}/admin/rech">
                                    <input type="text" class="form-control px-3 py-4" placeholder="Recherche" name="titre">
                                    <input type="submit" class="btn btn-primary  px-4 py-3" value="Rechercher">
                                </form>
                            </div>

                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-4 mb-4">
                        <p></p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <% for(int i = 0; i < info.size(); i++) { %>

            <div class="col-sm-6 col-lg-4 mb-4">
                <div class="blog-entry">
                    <a href="#"><img src="<%=info.get(i).getVisuel()%>" alt="Image placeholder" class="img-fluid"></a>
                    <div class="blog-entry-text">
                        <h3><a href="#"><%=info.get(i).getTitre()%></a></h3>
                        <p class="mb-4"><%=info.get(i).getBody()%></p>

                        <div class="meta">
                            <a href="#"><span class="icon-calendar"></span> <%=Contenue.formatDate(info.get(i).getDate1())%>
                                <% if(info.get(i).getDate2()!=null){ %> - <%=Contenue.formatDate(info.get(i).getDate2())%> <% } %>
                            </a>
                            <a href="#"><span class="icon-address"></span> <%=info.get(i).getLieu()%></a>
                        </div>
                        <a href="${pageContext.request.contextPath}/admin/validation/<%=info.get(i).getIdContenue()%>"><button>Valider le contenue</button></a>
                    </div>
                </div>
            </div>

            <% } %>

        </div>

    </div>
</section>

<%--	<section class="site-section" id="section-contact">--%>
<%--		<div class="container">--%>
<%--			<div class="row">--%>
<%--				<div class="col-md-12 mb-5">--%>
<%--					<div class="section-heading text-center">--%>
<%--						<h2>Get <strong>In Touch</strong></h2>--%>
<%--					</div>--%>
<%--				</div>--%>

<%--				<div class="col-md-7 mb-5 mb-md-0">--%>
<%--					<form action="" class="site-form">--%>
<%--						<h3 class="mb-5">Get In Touch</h3>--%>
<%--						<div class="form-group">--%>
<%--							<input type="text" class="form-control px-3 py-4" placeholder="Your Name">--%>
<%--						</div>--%>
<%--						<div class="form-group">--%>
<%--							<input type="email" class="form-control px-3 py-4" placeholder="Your Email">--%>
<%--						</div>--%>
<%--						<div class="form-group">--%>
<%--							<input type="email" class="form-control px-3 py-4" placeholder="Your Phone">--%>
<%--						</div>--%>
<%--						<div class="form-group mb-5">--%>
<%--							<textarea class="form-control px-3 py-4"cols="30" rows="10" placeholder="Write a Message"></textarea>--%>
<%--						</div>--%>
<%--						<div class="form-group">--%>
<%--							<input type="submit" class="btn btn-primary  px-4 py-3" value="Send Message">--%>
<%--						</div>--%>
<%--					</form>--%>
<%--				</div>--%>
<%--				<div class="col-md-5 pl-md-5">--%>
<%--					<h3 class="mb-5">My Contact Details</h3>--%>
<%--					<ul class="site-contact-details">--%>
<%--						<li>--%>
<%--							<span class="text-uppercase">Email</span>--%>
<%--							site@gmail.com--%>
<%--						</li>--%>
<%--						<li>--%>
<%--							<span class="text-uppercase">Phone</span>--%>
<%--							+30 976 1382 9921--%>
<%--						</li>--%>
<%--						<li>--%>
<%--							<span class="text-uppercase">Fax</span>--%>
<%--							+30 976 1382 9922--%>
<%--						</li>--%>
<%--						<li>--%>
<%--							<span class="text-uppercase">Address</span>--%>
<%--							San Francisco, CA <br>--%>
<%--							4th Floor8 Lower  <br>--%>
<%--							San Francisco street, M1 50F--%>
<%--						</li>--%>
<%--					</ul>--%>
<%--				</div>--%>
<%--			</div>--%>
<%--		</div>--%>
<%--	</section>--%>



<script src="${pageContext.request.contextPath}/resources/theme/js/vendor/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/theme/js/vendor/jquery-migrate-3.0.1.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/theme/js/vendor/popper.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/theme/js/vendor/bootstrap.min.js"></script>

<script src="${pageContext.request.contextPath}/resources/theme/js/vendor/jquery.easing.1.3.js"></script>

<script src="${pageContext.request.contextPath}/resources/theme/js/vendor/jquery.stellar.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/theme/js/vendor/jquery.waypoints.min.js"></script>

<script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js"></script>
<script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
<script src="js/custom.js"></script>

<!-- Google Map -->
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
    <script src="js/google-map.js"></script> -->

</body>
</html>