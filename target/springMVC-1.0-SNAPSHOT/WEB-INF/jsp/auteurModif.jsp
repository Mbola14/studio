<%@ page import="com.example.models.Types" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.example.models.Contenue" %>
<%
    ArrayList<Types> info=(ArrayList<Types>) request.getAttribute("type");
    Contenue cont=(Contenue) request.getAttribute("contenue");
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Rezume Free Template by Colorlib</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/theme/css/animate.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/theme/css/flexslider.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/theme/fonts/icomoon/style.css">

    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/theme/css/bootstrap.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/theme/css/style.css">

    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,700" rel="stylesheet">


</head>
<body data-spy="scroll" data-target="#pb-navbar" data-offset="200">

<div class="section-heading text-center">
    <div class="filters">
        <ul>
            <a href="${pageContext.request.contextPath}/auteur/insert"><li >Insertion</li></a>
            <a href="${pageContext.request.contextPath}/auteur"><li >Modification</li></a>
        </ul>
    </div>
</div>


<section class="site-section" id="section-contact">
    <div class="container">
        <div class="row">
            <div class="col-md-12 mb-5">
                <div class="section-heading text-center">
                    <h2> <strong>Modification de l'actualité</strong></h2>
                </div>
            </div>

            <div class="col-md-12 mb-5 mb-md-0">
                <form action="${pageContext.request.contextPath}/auteur/modifier" method="post" class="site-form" >
                    <input type="hidden" value="<%=cont.getVisuel()%>" name="visuel">
                    <input type="hidden" value="<%=cont.getIdEtat()%>" name="etat">
                    <input type="hidden" value="<%=cont.getPublication()%>" name="publication">
                    <input type="hidden" value="<%=cont.getAffiche()%>" name="affiche">
                    <input type="hidden" value="<%=cont.getIdContenue()%>" name="id">
                    <input type="hidden" value="<%=cont.getPriorite()%>" name="prio">
                    <input type="hidden" value="<%=cont.getIdAuteur()%>" name="auteur">
                    <div class="form-group">
                        <p>Types: <select id="type" name="idTypes" >
                            <option value="<%=cont.getIdTypes()%>">Types</option>
                            <% for (int i = 0; i < info.size(); i++) { %>
                            <option value="<%=info.get(i).getIdtypes()%>"><%=info.get(i).getNom()%></option>
                            <% } %>
                        </select></p>
                    </div>
                    <div class="form-group">
                        <p>Titre: <input name="titre" type="text" class="form-control px-3 py-4" value="<%=cont.getTitre()%>"></p>
                    </div>
                    <div class="form-group">
                        <p>Description: <input name="body" type="text" class="form-control px-3 py-4" value="<%=cont.getBody()%>"></p>
                    </div>
                    <div class="form-group">
                        <p>Date de debut: <input name="date1" type="text" class="form-control px-3 py-4" value="<%=cont.getDate1()%>"></p>
                    </div>
                    <div class="form-group">
                        <p>Date de fin: <input name="date2" id="date2" type="text" class="form-control px-3 py-4" value="<%=cont.getDate2()%>"></p>
                    </div>
                    <div class="form-group">
                        <p>Lieu: <input name="lieu" type="text" class="form-control px-3 py-4" value="<%=cont.getLieu()%>"></p>
                    </div>
                    <div class="form-group">
                        <div class="section-heading text-center">
                            <input type="submit" class="btn btn-primary  px-4 py-3" value="Modofier">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>



<script src="${pageContext.request.contextPath}/resources/theme/js/vendor/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/theme/js/vendor/jquery-migrate-3.0.1.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/theme/js/vendor/popper.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/theme/js/vendor/bootstrap.min.js"></script>

<script src="${pageContext.request.contextPath}/resources/theme/js/vendor/jquery.easing.1.3.js"></script>

<script src="${pageContext.request.contextPath}/resources/theme/js/vendor/jquery.stellar.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/theme/js/vendor/jquery.waypoints.min.js"></script>

<script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js"></script>
<script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
<script src="js/custom.js"></script>

<script>
    const imageCode= document.getElementById("imageCode");

    const tobase64=(file) => {
        return new Promise((resolve, reject) => {
            const fileReader=new FileReader();
            fileReader.readAsDataURL(file);

            fileReader.onload=()=>{
                resolve(fileReader.result);
            };

            fileReader.onerror=(error)=>{
                reject(error);
            };
        });
    };

    const uploadImage=async (event) => {
        const file =event.target.files[0];
        const base64=await tobase64(file);
        imageCode.value=base64;
        console.log(imageCode.value);
    }

    document.getElementById("selectImage").addEventListener("change",(e) =>{
        uploadImage(e);
    });

    function verifier(){
        var date2=document.getElementById('date2');
        var value=document.getElementById('type').value;
        if(value==2) {
            date2.disabled=false;
            date2.required= true;
        }
        else{
            date2.disabled=true;
            date2.required=false;
        }
    }


</script>


<!-- Google Map -->
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
    <script src="js/google-map.js"></script> -->

</body>
</html>