create database information;
alter database information owner to postgres;
\c information postgres;

create table admin(
    idAdmin serial primary key,
    nom varchar not null,
    mdp varchar not null
);
create table types(
    idTypes serial primary key,
    nom varchar not null
);

create table contenue(
    idContenue serial primary key,
    titre varchar not null,
    visuel varchar not null,
    body varchar not null,
    date1 timestamp not null,
    date2 timestamp ,
    lieu varchar not null,
    idTypes int not null,
    foreign key (idTypes) references types(idTypes)
);

insert into admin (nom,mdp)
values ('admin','admin');

insert into types (nom) values ('article');
insert into types (nom) values ('evenement');

select * from contenue;

update contenue set date1='2023-01-19' , date2='2023-01-31' where idContenue=7;

delete from contenue where idContenue<=5;

