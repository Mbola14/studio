package com.example.models;

import javax.persistence.*;

@Entity(name = "genre")
public class Genre {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "idgenre")
    private int idGenre;
    @Column(name = "sexe")
    private String sexe;

    public Genre(int idGenre, String sexe) {
        this.idGenre = idGenre;
        this.sexe = sexe;
    }

    public Genre() {
    }

    public int getIdGenre() {
        return idGenre;
    }

    public void setIdGenre(int idGenre) {
        this.idGenre = idGenre;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }
}
