package com.example.models;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "scene")
public class Scene {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "idscene")
    private int idScene;
    @Column(name = "idplateau")
    private int idPlateau;
    @Column(name = "duration")
    private int duration;
    @Column(name = "idtemps")
    private int idTemps;
    @Column(name = "idetat")
    private int idEtat;
    @Column(name = "planification")
    private Date planification;

    public Scene() {
    }

    public Scene(int idScene, int idPlateau, int duration, int idTemps, int idEtat, Date planification) {
        this.idScene = idScene;
        this.idPlateau = idPlateau;
        this.duration = duration;
        this.idTemps = idTemps;
        this.idEtat = idEtat;
        this.planification = planification;
    }

    public int getIdScene() {
        return idScene;
    }

    public void setIdScene(int idScene) {
        this.idScene = idScene;
    }

    public int getIdPlateau() {
        return idPlateau;
    }

    public void setIdPlateau(int idPlateau) {
        this.idPlateau = idPlateau;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getIdTemps() {
        return idTemps;
    }

    public void setIdTemps(int idTemps) {
        this.idTemps = idTemps;
    }

    public int getIdEtat() {
        return idEtat;
    }

    public void setIdEtat(int idEtat) {
        this.idEtat = idEtat;
    }

    public Date getPlanification() {
        return planification;
    }

    public void setPlanification(Date planification) {
        this.planification = planification;
    }
}
