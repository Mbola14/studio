package com.example.models;

import javax.persistence.*;

@Entity(name = "action")
public class Action {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "idaction")
    private int idAction;
    @Column(name = "idscene")
    private int idScene;
    @Column(name = "idacteur")
    private int idActeur;
    @Column(name = "dialogue")
    private String dialogue;

    public Action() {
    }

    public Action(int idAction, int idScene, int idActeur, String dialogue) {
        this.idAction = idAction;
        this.idScene = idScene;
        this.idActeur = idActeur;
        this.dialogue = dialogue;
    }

    public int getIdAction() {
        return idAction;
    }

    public void setIdAction(int idAction) {
        this.idAction = idAction;
    }

    public int getIdScene() {
        return idScene;
    }

    public void setIdScene(int idScene) {
        this.idScene = idScene;
    }

    public int getIdActeur() {
        return idActeur;
    }

    public void setIdActeur(int idActeur) {
        this.idActeur = idActeur;
    }

    public String getDialogue() {
        return dialogue;
    }

    public void setDialogue(String dialogue) {
        this.dialogue = dialogue;
    }
}
