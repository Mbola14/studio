package com.example.models;

import javax.persistence.*;

@Entity(name = "acteur")
public class Acteur {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "idacteur")
    private int idActeur;
    @Column(name = "nom")
    private String nom;
    @Column(name = "idgenre")
    private int idGenre;

    public Acteur(int idActeur, String nom, int idGenre) {
        this.idActeur = idActeur;
        this.nom = nom;
        this.idGenre = idGenre;
    }

    public Acteur() {
    }

    public int getIdActeur() {
        return idActeur;
    }

    public void setIdActeur(int idActeur) {
        this.idActeur = idActeur;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getIdGenre() {
        return idGenre;
    }

    public void setIdGenre(int idGenre) {
        this.idGenre = idGenre;
    }
}
