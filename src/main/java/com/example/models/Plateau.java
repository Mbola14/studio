package com.example.models;

import javax.persistence.*;

@Entity(name = "plateau")
public class Plateau {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "idplateau")
    private int idPlateau;
    @Column(name = "nom")
    private String nom;
    @Column(name = "latitude")
    private float latitude;
    @Column(name = "longitude")
    private float longitude;

    public Plateau() {
    }

    public Plateau(int idPlateau, String nom, float latitude, float longitude) {
        this.idPlateau = idPlateau;
        this.nom = nom;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public int getIdPlateau() {
        return idPlateau;
    }

    public void setIdPlateau(int idPlateau) {
        this.idPlateau = idPlateau;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }
}
