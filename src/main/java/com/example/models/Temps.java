package com.example.models;

import javax.persistence.*;

@Entity(name = "temps")
public class Temps {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "idtemps")
    private int idTemps;
    @Column(name = "periode")
    private String periode;

    public Temps(int idTemps, String periode) {
        this.idTemps = idTemps;
        this.periode = periode;
    }

    public int getIdTemps() {
        return idTemps;
    }

    public void setIdTemps(int idTemps) {
        this.idTemps = idTemps;
    }

    public String getPeriode() {
        return periode;
    }

    public void setPeriode(String periode) {
        this.periode = periode;
    }

    public Temps() {
    }
}
