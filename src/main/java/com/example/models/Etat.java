package com.example.models;

import javax.persistence.*;

@Entity(name = "etat")
public class Etat {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "idetat")
    private int idEtat;
    @Column(name = "etat")
    private String etat;

    public Etat() {
    }

    public Etat(int idEtat, String etat) {
        this.idEtat = idEtat;
        this.etat = etat;
    }

    public int getIdEtat() {
        return idEtat;
    }

    public void setIdEtat(int idEtat) {
        this.idEtat = idEtat;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }
}
