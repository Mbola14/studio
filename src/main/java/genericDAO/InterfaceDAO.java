package genericDAO;

import java.util.ArrayList;

public interface InterfaceDAO {

    public void delete(Object o)throws Exception;

    public Object findById(Object o)throws Exception;

    public void update(Object o)throws Exception;

    public ArrayList findAll(Object o)throws Exception;

    public void save(Object o)throws Exception;

}
