package genericDAO;

import javax.persistence.Id;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;

import java.util.*;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class HibernateDAO implements InterfaceDAO{

    private SessionFactory factory;
    private Session session;

    private static int pagination=6;


    public SessionFactory getFactory()throws Exception {
        return this.factory;
    }

    public void setFactory(SessionFactory factory) {
        this.factory = factory;
    }

    public Session getSession()throws Exception {
        if(this.session==null)
            this.session=this.factory.openSession();
        if(this.session.isOpen()==false )
            this.session=this.factory.openSession();
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public void delete(Object o)throws Exception{
        Session s=null;
        Transaction tran=null;
        try{
            s=this.getSession();
            tran=s.beginTransaction();
            s.remove(o);
            tran.commit();
        }
        catch (Exception e){
            if(tran!=null)
                tran.rollback();
            throw e;
        }
        finally {
            if(s!=null)
                s.close();
        }
    }

    public Object findById(Object o)throws Exception{
        Session s=null;
        try{
            s=this.getSession();
            Object id=this.getId(o);
            return s.get(o.getClass(),(Serializable) id);
        }
        catch (Exception e){
            throw e;
        }
        finally {
            if(s!=null)
                s.close();
        }
    }

    public void update(Object o)throws Exception{
        Session s=null;
        Transaction tran=null;
        try{
        s=this.getSession();
        tran=s.beginTransaction();
        s.update(o);
        tran.commit();
        }
        catch (Exception e){
        if(tran!=null)
        tran.rollback();
        throw e;
        }
        finally {
            if (s != null)
                s.close();
        }
    }

    public ArrayList findAll(Object o)throws Exception{
        Session s=null;
        try{
            s=this.getSession();
            CriteriaBuilder build=s.getCriteriaBuilder();
            CriteriaQuery query=build.createQuery(o.getClass());
            query.from(o.getClass());
            TypedQuery t = s.createQuery(query);
            List obj=(List)t.getResultList();
            return new ArrayList(obj);
        }
        catch (Exception e){
            throw e;
        }
        finally {
            if(s!=null)
                s.close();
        }
    }

    public ArrayList findAll(Object o,int pagination)throws Exception{
        Session s=null;
        int offset=(pagination-1)*HibernateDAO.pagination;
        try{
            s=this.getSession();
            CriteriaBuilder build=s.getCriteriaBuilder();
            CriteriaQuery query=build.createQuery(o.getClass());
            query.from(o.getClass());
            TypedQuery t = s.createQuery(query);
            t.setFirstResult(offset);
            t.setMaxResults(HibernateDAO.pagination);
            List obj=(List)t.getResultList();
            return new ArrayList(obj);
        }
        catch (Exception e){
            throw e;
        }
        finally {
            if(s!=null)
                s.close();
        }
    }

    public void save(Object o)throws Exception{
        Session s=null;
        Transaction tran=null;
        try{
            s=this.getSession();
            tran=s.beginTransaction();
            s.save(o);
            tran.commit();
        }
        catch (Exception e){
            if(tran!=null)
                tran.rollback();
            throw e;
        }
        finally {
            if(s!=null)
                s.close();
        }
    }

    private Object getId(Object obj)throws Exception{
        Class<?> modelClass=obj.getClass();
        Field[] modelFields=modelClass.getDeclaredFields();
        for (int i = 0; i < modelFields.length; i++) {
            if(modelFields[i].getDeclaredAnnotation(Id.class)==null)
                continue;
            char[] nomfield=modelFields[i].getName().toCharArray();
            nomfield[0]=Character.toUpperCase(nomfield[0]);
            String nomFonction=new String(nomfield);
            Method met=obj.getClass().getDeclaredMethod("get"+nomFonction, null);
            Object val=met.invoke(obj, null);
            return val;
        }
        return null;
    }
}
