-- create database studio;
-- alter database studio owner to postgres;
-- \c studio postgres;


-- create database studio;
-- \c studio;

DROP TABLE genre CASCADE;
DROP TABLE etat CASCADE;
DROP TABLE temps CASCADE;
DROP TABLE acteur CASCADE;
DROP TABLE plateau CASCADE;
DROP TABLE scene CASCADE;
DROP TABLE action CASCADE;

create table genre(
                      idGenre serial primary key,
                      sexe varchar not null
);
insert into genre(sexe) values ('homme');
insert into genre(sexe) values ('femme');

create table etat(
                     idEtat serial primary key,
                     etat varchar not null
);
insert into etat(etat) values ('a faire');
insert into etat(etat) values ('planifier');
insert into etat(etat) values ('fini');

create table temps(
                      idTemps serial primary key,
                      periode varchar not null
);
insert into temps(periode) values ('aube');
insert into temps(periode) values ('jour');
insert into temps(periode) values ('crepuscule');
insert into temps(periode) values ('nuit');

create table acteur(
                       idActeur serial primary key,
                       nom varchar not null,
                       idgenre int not null,
                       foreign key(idgenre) references genre(idgenre)
);

create table plateau(
                        idPlateau serial primary key,
                        nom varchar not null,
                        latitude float not null,
                        longitude float not null
);

create table scene(
                      idScene serial primary key,
                      idPlateau int not null,
                      duration int not null,
                      idTemps int,
                      idEtat int not null default 1,
                      planification timestamp,
                      foreign key (idPlateau) references plateau(idPlateau),
                      foreign key (idTemps) references temps(idTemps)
);

create table action(
                       idAction serial primary key,
                       idScene int not null,
                       idActeur int,
                       dialogue varchar,
                       foreign key(idScene) references scene(idScene),
                       foreign key(idActeur) references acteur(idActeur)
);

create table indisponnible(
    idIndisponnible serial primary key,
    idPlateau int not null,
    debut date not null,
    fin date not null,
    foreign key(idPlateau) references plateau(idPlateau)
);
create table indispoActeur(
    idindispoActeur serial primary key,
    idActeur int not null,
    debut date not null,
    fin date not null,
    foreign key(idActeur) references acteur(idActeur)
);

create table ferie(
                      idFerie serial primary key,
                      nom varchar not null,
                      dateFerie date not null
);


insert into ferie (nom, dateFerie) values
                                       ('Nouvel An', '2023-01-01'),
                                       ('Fete de l independance', '2023-06-26'),
                                       ('Noel', '2023-12-25');
insert into ferie (nom, dateFerie)
values ('test ferie','2023-03-20');




INSERT INTO plateau (nom, latitude, longitude) VALUES
                                                   ('Plateau 1', -18.902718919026928, 47.510238818036534),
                                                   ('Plateau 2', -18.907007336366256, 47.52591632883213),
                                                   ('Plateau 3', -18.898699379805674, 47.529765298797805),
                                                   ('Plateau 4', -18.901785095271396, 47.540236642098236),
                                                   ('Plateau 5', -18.884203876229865, 47.52581708738944);

insert into indisponnible (idPlateau, debut, fin)
values (1,'2023-03-21','2023-03-22');

INSERT INTO scene (idPlateau, duration, idTemps) VALUES
                                                     (1, 120, 2),
                                                     (1, 30, 3),
                                                     (1, 45, 2);

INSERT INTO scene (idPlateau, duration, idTemps, idEtat, planification) VALUES
                                                                            (2, 60, 2, 2, '2023-03-28 10:00:00'),
                                                                            (2, 90, 2, 2, '2023-03-28 11:15:00'),
                                                                            (1, 35, 4, 2, '2023-03-28 20:30:00');


insert into Acteur(nom, idGenre) values
                                     ('Lucka', 1),
                                     ('Mbola', 1);

insert into action (idScene, idActeur, dialogue)
values (5,1,'gfdfg');

insert into indispoActeur (idActeur, debut, fin)
values (1,'2023-03-21','2023-03-21');

create or replace view v_scene as
    select s.*,p.nom,t.periode from scene s join plateau p on s.idPlateau=p.idPlateau join temps t on  t.idTemps=s.idTemps order by idScene;

insert into scene (idPlateau, duration, idTemps, idEtat)
values (3,60,2,1);

insert into scene (idPlateau, duration, idTemps, idEtat)
values (3,160,null,1);