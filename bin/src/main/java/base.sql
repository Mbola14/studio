create database information;
alter database information owner to postgres;
\c information postgres;

create table pagination(
    id int primary key,
    nombre int not null
);
insert into pagination values (1,6);

create table auteur(
    idAuteur serial primary key ,
    nom varchar not null,
    mdp varchar not null
);

create table etat(
    idEtat serial primary key ,
    nomEtat varchar not null
);

create table admin(
    idAdmin serial primary key,
    nom varchar not null,
    mdp varchar not null
);
create table types(
    idTypes serial primary key,
    nom varchar not null
);

create table contenue(
    idContenue serial primary key,
    titre varchar not null,
    visuel varchar not null,
    body varchar not null,
    date1 timestamp not null,
    date2 timestamp ,
    lieu varchar not null,
    idTypes int not null,
    idEtat int default 1,
    foreign key (idTypes) references types(idTypes),
    foreign key (idEtat) references etat(idEtat)
);

alter table contenue add column idEtat int default 1;
alter table contenue add foreign key(idEtat) references etat(idEtat);
alter table contenue add column idAuteur int not null default 1;
alter table contenue add foreign key(idAuteur) references auteur(idAuteur);

alter table contenue add column publication timestamp default '2023-02-01';

alter table contenue add column affiche int default 0;

alter table contenue add column priorite int default 1;

insert into etat(nomEtat) values ('cree'),('publie');

insert into admin (nom,mdp)
values ('admin','admin');

insert into auteur (nom, mdp) values ('Bema','bema');
insert into auteur (nom, mdp) values ('admin','admin');


insert into types (nom) values ('article');
insert into types (nom) values ('evenement');

select * from contenue;
select * from etat;

update contenue set date1='2023-01-19' , date2='2023-01-31' where idContenue=7;

delete from contenue where idContenue=32;


