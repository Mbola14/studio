<%@ page import="com.example.models.Contenue" %>
<%@ page import="java.util.ArrayList" %>
<%
	ArrayList<Contenue> info=(ArrayList<Contenue>) request.getAttribute("info");
	int pagination=(int) request.getAttribute("pagination");
%>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Rezume Free Template by Colorlib</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/theme/css/animate.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/theme/css/flexslider.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/theme/fonts/icomoon/style.css">

	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/theme/css/bootstrap.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/theme/css/style.css">

	<link href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,700" rel="stylesheet">


</head>
<body data-spy="scroll" data-target="#pb-navbar" data-offset="200">

	<section class="site-section" id="section-blog">
		<div class="container">
			<div class="row">
				<div class="col-md-12 mb-5">
					<div class="section-heading text-center">
						<h2><strong>Les actualités</strong></h2>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12 mb-5">
					<div class="row">
						<div class="col-sm-6 col-lg-4 mb-4">
							<p></p>
						</div>
						<div class="col-sm-6 col-lg-4 mb-4">
							<div class="section-heading text-center">

							<div class="form-group">
								<form method="post" action="rech">
								<input type="text" class="form-control px-3 py-4" placeholder="Recherche" name="titre">
								<input type="submit" class="btn btn-primary  px-4 py-3" value="Rechercher">
								</form>
							</div>

							</div>
						</div>
						<div class="col-sm-6 col-lg-4 mb-4">
							<p></p>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<% for(int i = 0; i < info.size(); i++) { %>

				<div class="col-sm-6 col-lg-4 mb-4">
					<div class="blog-entry">
						<a href="#"><img src="<%=info.get(i).getVisuel()%>" alt="Image placeholder" class="img-fluid"></a>
						<div class="blog-entry-text">
							<% if(info.get(i).getIdTypes()==2){ %>
							<h3 style="color: blue" ><%=info.get(i).getTitre()%></h3>
							<% } else {%>
							<h3 ><%=info.get(i).getTitre()%></h3>
							<% } %>
							<p class="mb-4"><%=info.get(i).getBody()%> </p>

							<div class="meta">
								<a href="#"><span class="icon-calendar"></span> <%=Contenue.formatDate(info.get(i).getDate1())%>
								<% if(info.get(i).getDate2()!=null){ %> - <%=Contenue.formatDate(info.get(i).getDate2())%> <% } %> </a>
								<a href="#"><span class="icon-address"></span> <%=info.get(i).getLieu()%></a>
							</div>
						</div>
					</div>
				</div>

				<% } %>

			</div>

			<div class="row">
				<div class="col-md-12 mb-5">
					<div class="section-heading text-center">
						<div class="filters">
							<ul>
								<% for(int i = 1; i <= pagination; i++) { %>
								<a href="<%=i%>"><li ><%=i%></li></a>
								<% } %>
							</ul>
						</div>
					</div>
				</div>
			</div>

		</div>
	</section>


	<script src="${pageContext.request.contextPath}/resources/theme/js/vendor/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/theme/js/vendor/jquery-migrate-3.0.1.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/theme/js/vendor/popper.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/theme/js/vendor/bootstrap.min.js"></script>

	<script src="${pageContext.request.contextPath}/resources/theme/js/vendor/jquery.easing.1.3.js"></script>

	<script src="${pageContext.request.contextPath}/resources/theme/js/vendor/jquery.stellar.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/theme/js/vendor/jquery.waypoints.min.js"></script>

	<script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js"></script>
	<script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
	<script src="js/custom.js"></script>

	<!-- Google Map -->
    <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
    	<script src="js/google-map.js"></script> -->

    </body>
    </html>